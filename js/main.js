$(window).load(function(){
   $('.page-loader').fadeOut();
});
$(document).ready(function(){
    var winWidth = $(window).width();
    var winHeight = $(window).height();
    var navContent = $('.nav-container').length;
    var searchMobile = $('#search-mobile').length;
    var filterMobile = $('#filter-mobile').length;
    if (winWidth > 768) {
		stickyTop();
		stickyNav();
	}
	if (winWidth < 769) {
		if (navContent) {
			$('ul.nav.navbar-nav').remove().clone().appendTo('#sidenav');
			if (searchMobile) {
				$('.banner-home').find('.filter-home').remove();
				$('.left-top-nav').find('.filter-home').remove().clone().appendTo('#search-mobile');
			}
			if (filterMobile) {
				$('.produk-filter-content').find('.produk-filter').remove().clone().appendTo('#filter-mobile');

				$(".nav-tabs a").click(function(){
					$(this).tab('show');
				});
			}

			$(window).scroll(function() {
				var topPos = this.pageYOffset,
					posFooter = $('.footer').offset().top,
					topTotal = posFooter-topPos
				;
				if (topTotal < 600) {
					$('.toggle-search-mobile').hide();
				} else {
					$('.toggle-search-mobile').show();
				}
			});
		}
	}
});
$('.tab-filter a, .tab-filter label').click(function(){
    $('.tab-filter li').removeClass('active');
    $(this).parent().addClass('active');
});
$('select[name="filter-category"]').on('change', function() {
	var value = $(this).val();
	$('[data-label="'+value+'"]').trigger('click');
});
$('.filter-umroh').on('click', function() {
	$('.select-lokasi').addClass('hide');
	$('.select-paket').removeClass('hide');
});
$('.filter-wisata').on('click', function() {
	$('.select-paket').addClass('hide');
	$('.select-lokasi').removeClass('hide');
});
$('.toggle-search-mobile').on('click touch', function() {
	$('.search-mobile').toggleClass('show-search');
	if($('.search-mobile').hasClass('show-search')) {
		$('body').addClass('no-overflow');
	}
});
$('.closebtnsearch').on('click touch', function() {
	$('.search-mobile').toggleClass('show-search');
	$('body').removeClass('no-overflow');
});
$('#triggerAvatar').on('click', function() {
	$('#inputAvatar').trigger('click');
});

function readImageUpload(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#image-preview-img').attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
	}
}

$("#inputAvatar").change(function() {
  readImageUpload(this);
});

function openNav() {
	$('body').addClass('open-nav-side');
	$('body').prepend('<div class="bg-overlay"/>');
	$('.bg-overlay').on('click', function() {
		closeNav();
	});
}

function closeNav() {
	$('body').removeClass('open-nav-side');
	$('.bg-overlay').remove();
}

function stickyTop() {
	$(window).scroll(function() {
		var scrollHead = $(window).scrollTop(),
			topHeight = $('.top-container').outerHeight(),
			bannerHeight = $('.banner-home').outerHeight();

		if (scrollHead > bannerHeight+51) {
			$('.top-container').addClass('stickyTop');
		} else {
			$('.top-container').removeClass('stickyTop');
		}
	});
}
function stickyNav() {
	$(window).scroll(function() {
		var scrollHead = $(window).scrollTop(),
			bannerHeight = $('.banner-home').outerHeight();
		var filterHome = $('.filter-home').length;
		if (scrollHead > bannerHeight+51) {
			$('.nav-container').addClass('stickyNav');
			if (filterHome) {
				$('.left-top-nav').addClass('visible');
			}
		} else {
			$('.nav-container').removeClass('stickyNav');
			if (filterHome) {
				$('.left-top-nav').removeClass('visible');
				$('.bg-left-nav').remove();
			}
		}
	});
}

$('.read-more-nav').click(function(){
	$(this).parent().toggleClass('full-mobile');
	$(this).toggleClass('bottom');
});





